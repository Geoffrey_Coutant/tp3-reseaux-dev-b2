import os
from sys import argv
from _socket import gethostbyname
from psutil import net_if_addrs


def lookup(domain: str) -> str:
    return gethostbyname(domain)


def ping(ip: str) -> str:
    return "UP !" if os.system(f"ping {ip} > NUL") == 0 else "DOWN !"


def ip() -> str:
    return net_if_addrs()["Wi-Fi"][1].address


v = ""
match argv[1]:
    case "lookup":
        hostname = lookup(argv[2])
    case "ping":
        hostname = ping(argv[2])
    case "ip":
        hostname = ip()
    case _:
        hostname = f"'{argv[1]}' n'est pas disponible"

print(hostname)
