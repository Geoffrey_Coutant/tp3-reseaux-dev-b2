# TP3 DEV : Python et réseau

## I. Ping

### 🌞ping_simple.py

***Code :***
```
import os
def ping(ip: str):
    return os.system(f"ping {ip}")

ping("8.8.8.8")
```

***Réponse***
```
/usr/local/bin/python3.12 /Users/radiou22/Documents/tp3-reseaux-dev-b2/I-ping/ping_simple.py 
PING 8.8.8.8 (8.8.8.8): 56 data bytes
64 bytes from 8.8.8.8: icmp_seq=0 ttl=112 time=26.299 ms
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=40.423 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=112 time=24.750 ms

--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 24.750/30.491/40.423/7.052 ms

Process finished with exit code 0
```

### 🌞 ping_arg.py

***Code :***

```
import os

hostname = "8.8.8.8"
reponse = os.system("ping -c 1 " + hostname)
```

***Réponse***

```
/usr/local/bin/python3.12 /Users/radiou22/Documents/tp3-reseaux-dev-b2/ping_arg.py 
PING 8.8.8.8 (8.8.8.8): 56 data bytes
64 bytes from 8.8.8.8: icmp_seq=0 ttl=114 time=20.499 ms

--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 20.499/20.499/20.499/nan ms

Process finished with exit code 0
```

### 🌞is_up.py

***Code :***

```
import os

hostname = "8.8.8.8"
reponse = os.system("ping -c 1 " + hostname)

if(reponse == 0):
    print("UP !")
else:
    print("DOWN !")
```
***Réponse***
```
/usr/local/bin/python3.12 /Users/radiou22/Documents/tp3-reseaux-dev-b2/is_up.py 
PING 8.8.8.8 (8.8.8.8): 56 data bytes
64 bytes from 8.8.8.8: icmp_seq=0 ttl=114 time=19.454 ms

--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 19.454/19.454/19.454/nan ms
UP !

Process finished with exit code 0
```

## II. DNS

### 🌞 lookup.py

***code :***

```
import socket
import sys

hostname = sys.argv[1]

print(f'The {hostname} IP address is {socket.gethostbyname(hostname)}')
```

***Réponse***

```
radiou22@Air-de-jojo ~ % python3 Documents/tp3-reseaux-dev-b2/lookup.py ynov.com
The ynov.com IP address is 172.67.74.226
```

## III. Get your IP

### 🌞 get_ip.py

***Code :***
```
from psutil import net_if_addrs
hostname = net_if_addrs()

print(hostname["en0"][0].address)
```

***Réponse :***
```
/usr/local/bin/python3.12 /Users/radiou22/Documents/tp3-reseaux-dev-b2/get_ip.py 
10.33.76.3

Process finished with exit code 0
```

## IV. Mix

### 🌞 network.py

