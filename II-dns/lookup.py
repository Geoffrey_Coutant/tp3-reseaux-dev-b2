import socket
import sys

hostname = sys.argv[1]

print(f'The {hostname} IP address is {socket.gethostbyname(hostname)}')